package ca.neil.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculateInfo(View v){
        EditText loanET = (EditText) findViewById(R.id.totalLoan);
        EditText termLoanET = (EditText) findViewById(R.id.numYears);
        EditText yearIntET = (EditText) findViewById(R.id.yearsInt);
        clearBottomLayout(v);
        validInputCalc(loanET.getText().toString(), termLoanET.getText().toString(), yearIntET.getText().toString(), v);
    }

    public void clearTxt(View v){
        clearBottomLayout(v);
        clearTopLayout(v);
    }

    private void clearBottomLayout(View v){
        TextView errTV = (TextView) findViewById(R.id.errorEmptyTxt);
        TextView errTV2 = (TextView) findViewById(R.id.errorNonNumsTxt);
        errTV.setText("");
        errTV2.setText("");
        TextView mthPayResultET = (TextView) findViewById(R.id.mthPayResult);
        TextView totPayResultET = (TextView) findViewById(R.id.totPayResult);
        TextView totIntResultET = (TextView) findViewById(R.id.totIntResult);
        mthPayResultET.setText("0.0");
        totPayResultET.setText("0.0");
        totIntResultET.setText("0.0");
    }

    private void clearTopLayout(View v){
        EditText loanET = (EditText) findViewById(R.id.totalLoan);
        EditText termLoanET = (EditText) findViewById(R.id.numYears);
        EditText yearIntET = (EditText) findViewById(R.id.yearsInt);
        loanET.setText("");
        termLoanET.setText("");
        yearIntET.setText("");
    }


    private void printResults(LoanCalculator lc, View v){
        clearBottomLayout(v);
        TextView mthPayResultET = (TextView) findViewById(R.id.mthPayResult);
        TextView totPayResultET = (TextView) findViewById(R.id.totPayResult);
        TextView totIntResultET = (TextView) findViewById(R.id.totIntResult);
        mthPayResultET.setText((Math.round(lc.getMonthlyPayment()*100)/100.0) + "");
        totPayResultET.setText((Math.round(lc.getTotalCostOfLoan()*100)/100.0) + "");
        totIntResultET.setText((Math.round(lc.getTotalInterest()*100)/100.0) + "");
    }

    private void validInputCalc(String a, String b, String c, View v){
        TextView errTV = (TextView) findViewById(R.id.errorEmptyTxt);
        TextView errTV2 = (TextView) findViewById(R.id.errorNonNumsTxt);
        Double loan, yearInt;
        int termsLoan;
        if(a.length() == 0 || b.length() == 0 || c.length() == 0) {
            errTV.setText(R.string.errorMsgTxt);
        }else {
            try{
                loan = Double.parseDouble(a);
                termsLoan = (int)Double.parseDouble(b);
                yearInt = Double.parseDouble(c);
                LoanCalculator lc = new LoanCalculator(loan, termsLoan, yearInt);
                printResults(lc,v);

            }catch(Exception e){
                errTV2.setText(R.string.nonNumErrorMsgTxt);
            }
        }
    }
}
